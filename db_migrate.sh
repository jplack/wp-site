#! /bin/bash

echo -n "Old URL (localhost:8000): "
read OLD_URL
OLD_URL=${OLD_URL:="localhost:8000"}
echo -n "New URL: "
read NEW_URL;
echo -n "RDS Endpoint: "
read RDS_ENDPOINT;
echo -n "RDS Root Password: "
read RDS_ROOT_PW;

if [ -z "$NEW_URL" ]
then
      echo "You must set a New URL."
      exit 1
elif [ -z "$RDS_ENDPOINT" ]
then
      echo "You must provide an RDS Endpoint."
      exit 1
elif [ -z "$RDS_ROOT_PW" ]
then
     echo "You must provide a RDS Root Password."
     exit 1
fi

DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd | rev | cut -d '/' -f1 | rev )"
CONTAINER_NAME="${DIRECTORY//-/}_db_1"
DATABASE=$( echo $( cat "./docker-compose.yml" | sed -n -e 's/^.*MYSQL_DATABASE: //p' ) )
ROOT_PW=$( echo $( cat "./docker-compose.yml" | sed -n -e 's/^.*MYSQL_ROOT_PASSWORD: //p' ) )
DUMP_FILE="${DATABASE}_$( date +%Y-%m-%d-%Hh%Mm%Ss ).sql"

docker exec "$CONTAINER_NAME" /usr/bin/mysqldump -u root --password="$ROOT_PW" "$DATABASE" > "$DUMP_FILE"

sed -i "" "s/${OLD_URL}/${NEW_URL}/g" "./${DUMP_FILE}"

mysql -u root \
    --port=3306 \
    --host="${RDS_ENDPOINT}" \
    "-p${RDS_ROOT_PW}" \
    --database="${DATABASE}" < "./${DUMP_FILE}";

exit 0
